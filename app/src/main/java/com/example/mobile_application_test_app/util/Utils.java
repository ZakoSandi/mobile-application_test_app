package com.example.mobile_application_test_app.util;

import android.support.design.widget.Snackbar;
import android.text.Html;
import android.view.View;

public class Utils {

    public static void showSnack(View view, String text) {
        Snackbar.make(view, Html.fromHtml("<font color=\"#ffffff\">" + text + "</font>"), Snackbar.LENGTH_SHORT).setDuration(4000).show();
    }

}
