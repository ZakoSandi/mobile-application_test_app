package com.example.mobile_application_test_app;

public class AppConstants {

    public final static String PAGE="page";
    public final static String MAXPAGE="maxpage";
    public final static String APPNAME="appname";
    public final static String APPTYPE="apptype";
    public final static String STORELINK="storelink";
    public final static String IMAGEURL="imgurl";
    public final static String APPDETAILS1="appdetails1";
    public final static String APPDETAILS2="appdetails2";
}
