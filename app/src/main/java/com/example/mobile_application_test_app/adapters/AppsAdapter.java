package com.example.mobile_application_test_app.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.mobile_application_test_app.R;
import com.example.mobile_application_test_app.remote.Models.Item;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import java.util.Locale;

public class AppsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int ROW_TAG = 12345;
    final Context context;
    private List<Item> mAppList;
    private List<Item> tmpAppList;
    protected AppsAdapter.OnClickListener mOnClickListener;

    public AppsAdapter(Context context, List<Item> appPageList) {
        this.context = context;
        this.mAppList = appPageList;
        this.tmpAppList = new ArrayList<Item>();
        this.tmpAppList.addAll(mAppList);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.app_item, viewGroup, false);
        v.setOnClickListener(this);
        v.setTag(R.id.item_id, i);
        return new AppsAdapter.AppsAdapterHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof AppsAdapter.AppsAdapterHolder) {
            bindVH((AppsAdapter.AppsAdapterHolder) viewHolder, i);
        }
    }

    public void bindVH(AppsAdapterHolder viewHolder, int position) {
        Context context = viewHolder.img_app.getContext();
        Item app = mAppList.get(position);
        Glide.with(context).load(app.getImage()).into(viewHolder.img_app);
        viewHolder.tv_titleApp.setText(app.getName());
        viewHolder.itemView.setTag(R.id.item_id, ROW_TAG);
        viewHolder.itemView.setTag(R.id.seznam_id, app);
        viewHolder.itemView.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        if (mAppList == null) {
            Toast.makeText(context, R.string.no_loaded_warning, Toast.LENGTH_SHORT).show();
            return 0;
        } else {
            return (mAppList != null && mAppList.size() != 0) ?
                    mAppList.size() : 0;
        }
        //return mAppList.size();
    }

    @Override
    public void onClick(View view) {
        if (mOnClickListener != null) {
            switch ((int) view.getTag(R.id.item_id)) {
                case ROW_TAG:
                    mOnClickListener.onDetailsClick(view, (Item) view.getTag(R.id.seznam_id));
                    break;
            }
        }
    }

    public class AppsAdapterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_app)
        ImageView img_app;
        @BindView(R.id.titleApp)
        TextView tv_titleApp;

        public AppsAdapterHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setOnClickListener(AppsAdapter.OnClickListener listener) {
        mOnClickListener = listener;
    }

    public interface OnClickListener {
        void onDetailsClick(View v, Item item);
    }

    // Filter Items depends on typed query
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mAppList.clear();
        if (charText.length() == 0) {
            mAppList.addAll(tmpAppList);
        } else {
            for (Item item : tmpAppList) {
                if (item.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    mAppList.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }

    // Filter Items depends on selected type
    public void filterType(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mAppList.clear();
        if (charText.length() == 0) {
            mAppList.addAll(tmpAppList);
        } else {
            if (charText.equals("all")) {
                mAppList.addAll(tmpAppList);
            } else {

                for (Item item : tmpAppList) {
                    if (item.getType().toLowerCase(Locale.getDefault()).contains(charText)) {
                        mAppList.add(item);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

}
