package com.example.mobile_application_test_app.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;

import com.example.mobile_application_test_app.AppConstants;
import com.example.mobile_application_test_app.MainActivity;
import com.example.mobile_application_test_app.R;
import com.example.mobile_application_test_app.adapters.AppsAdapter;
import com.example.mobile_application_test_app.remote.Models.Item;
import com.example.mobile_application_test_app.remote.Models.Page;
import com.example.mobile_application_test_app.remote.REST;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AppShowFragment extends Fragment {

    private Unbinder unbinder;
    @BindView(R.id.my_recycler_view)
    RecyclerView recyclerView;
    AppsAdapter appsAdapter;
    @BindView(R.id.searchview)
    android.widget.SearchView searchview;
    @BindView(R.id.radioAll)
    RadioButton radioAll;
    @BindView(R.id.radioAndroid)
    RadioButton radioAndroid;
    @BindView(R.id.radioIos)
    RadioButton radioIos;
    @BindView(R.id.groupRadios)
    RadioGroup groupRadios;

    REST.getApps getAppCall;
    Retrofit retrofit;
    List<Item> appList;
    Page page;
    int pageNumber = 1;
    String searchQuery = "";

    public AppShowFragment() {
    }

    public static Fragment newInstance(@Nullable Bundle bundle) {
        AppShowFragment appShowFragment = new AppShowFragment();
        appShowFragment.setArguments(bundle);
        return appShowFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_app_show, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (getArguments() != null && getArguments().containsKey(AppConstants.PAGE)) {
            pageNumber = getArguments().getInt(AppConstants.PAGE);
        }

        loadData();
        radioChecking();

        searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchQuery = s.toLowerCase();
                if (appsAdapter != null)
                    appsAdapter.filter(searchQuery);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                searchQuery = s.toLowerCase();

                if (appsAdapter != null)
                    appsAdapter.filter(searchQuery);
                return false;
            }
        });

        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        loadData();
        super.onResume();
    }

    public void loadData() {

        retrofit = REST.getClient();

        getAppCall = retrofit.create(REST.getApps.class);

        Call<Page> myApps = getAppCall.getAppsByPage(pageNumber);

        myApps.enqueue(new Callback<Page>() {
            @Override
            public void onResponse(Call<Page> call, Response<Page> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    page = response.body();
                    appList = page.getItems();

                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.MAXPAGE, page.getMaxPages());
                    Log.d("SANDI", "max pages: " + page.getMaxPages());

                    for (Item item : appList) {
                        Log.d("SANDI", item.getName());
                    }

                    if (appList.size() > 0) {
                        appsAdapter = new AppsAdapter(getActivity(), appList);
                        if (appsAdapter != null) {
                            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                            recyclerView.setAdapter(appsAdapter);

                            appsAdapter.setOnClickListener(new AppsAdapter.OnClickListener() {
                                @Override
                                public void onDetailsClick(View v, Item item) {
                                    Log.d("SANDI", "clicked");
                                    if (item != null) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString(AppConstants.APPNAME, item.getName());
                                        bundle.putString(AppConstants.IMAGEURL, item.getImage());
                                        bundle.putString(AppConstants.APPTYPE, item.getType());
                                        if (item.getType().equals("android")) {
                                            bundle.putString(AppConstants.STORELINK, item.getAndroidStore());
                                            bundle.putDouble(AppConstants.APPDETAILS1, item.getAndroidDetails().getRatings());
                                            bundle.putDouble(AppConstants.APPDETAILS2, item.getAndroidDetails().getScore());
                                        } else {
                                            bundle.putString(AppConstants.STORELINK, item.getIosStore());
                                            bundle.putDouble(AppConstants.APPDETAILS1, item.getIosDetails().getSize());
                                            bundle.putDouble(AppConstants.APPDETAILS2, 0.0);
                                        }
                                        ((MainActivity) getActivity()).addFragment(AppDetailsFragment.class, bundle, true, true);
                                    }
                                }
                            });
                        }
                    } else {
                        recyclerView.setVisibility(View.GONE);
                    }
                } else if (response.code() == 500) {
                    new AlertDialog.Builder(getContext())
                            .setTitle(getString(R.string.no_loaded_warning))
                            .setMessage(getString(R.string.instructions_for_data))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    loadData();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<Page> call, Throwable t) {
                new AlertDialog.Builder(getContext())
                        .setTitle(getString(R.string.no_loaded_warning))
                        .setMessage(getString(R.string.instructions_for_data))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                loadData();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    public void radioChecking() {

        groupRadios.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.radioAll:
                        appsAdapter.filterType(radioAll.getText().toString());
                        break;
                    case R.id.radioAndroid:
                        appsAdapter.filterType(radioAndroid.getText().toString());
                        break;
                    case R.id.radioIos:
                        appsAdapter.filterType(radioIos.getText().toString());
                        break;
                }
            }
        });

    }

}
