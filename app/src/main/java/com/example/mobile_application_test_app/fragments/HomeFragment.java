package com.example.mobile_application_test_app.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mobile_application_test_app.AppConstants;
import com.example.mobile_application_test_app.R;
import com.example.mobile_application_test_app.pager_adapters.AppsPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HomeFragment extends Fragment {

    private Unbinder unbinder;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.tabs_container)
    TabLayout tabs_container;
    int num_of_pages = -1;
    AppsPagerAdapter adapter;

    public HomeFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (getArguments() != null && getArguments().containsKey(AppConstants.MAXPAGE)) {
            num_of_pages = getArguments().getInt(AppConstants.MAXPAGE);
            //Log.d("SANDI", "max pages: " + num_of_pages);
        }

        setupViewPager(pager, num_of_pages);
        tabs_container.setupWithViewPager(pager);

        tabs_container.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
                //adapter.notifyDataSetChanged();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        return view;
    }

    private void setupViewPager(ViewPager viewPager, int maxPages) {
        adapter = new AppsPagerAdapter(getChildFragmentManager(), num_of_pages);
        for (int i = 0; i < maxPages + 1; i++) {
            Bundle bundle = new Bundle();
            bundle.putInt(AppConstants.PAGE, i);
            adapter.addFragment(AppShowFragment.newInstance(bundle), getString(R.string.page_text)+ " " + (i + 1));
        }

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

}
