package com.example.mobile_application_test_app.fragments;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mobile_application_test_app.AppConstants;
import com.example.mobile_application_test_app.R;
import com.example.mobile_application_test_app.util.Utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rebus.permissionutils.AskAgainCallback;
import rebus.permissionutils.FullCallback;
import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionManager;
import rebus.permissionutils.PermissionUtils;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AppDetailsFragment extends Fragment {

    private Unbinder unbinder;

    @BindView(R.id.app_name)
    TextView app_name;
    @BindView(R.id.tv_appName)
    TextView tv_appName;
    @BindView(R.id.tv_appType)
    TextView tv_appType;
    @BindView(R.id.tv_appStoreLink)
    TextView tv_appStoreLink;
    @BindView(R.id.app_img)
    ImageView app_img;
    @BindView(R.id.btn_back)
    TextView btn_back;
    @BindView(R.id.tv_appDetails)
    TextView tv_appDetails;
    @BindView(R.id.tv_appDetails2)
    TextView tv_appDetails2;

    String appName, imageURL, appType, storeLink;
    double appDetails, appDetails2;

    @BindView(R.id.tv_downloadImg)
    TextView tv_downloadImg;

    private static final int PERMISSION_REQUEST_CODE = 200;

    public AppDetailsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_app_details, container, false);

        unbinder = ButterKnife.bind(this, view);

        if (getArguments() != null && getArguments().containsKey(AppConstants.APPNAME)) {
            appName = getArguments().getString(AppConstants.APPNAME);
            imageURL = getArguments().getString(AppConstants.IMAGEURL);
            appType = getArguments().getString(AppConstants.APPTYPE);
            storeLink = getArguments().getString(AppConstants.STORELINK);
            appDetails = getArguments().getDouble(AppConstants.APPDETAILS1);
            appDetails2 = getArguments().getDouble(AppConstants.APPDETAILS2);
        }

        tv_downloadImg.setPaintFlags(tv_downloadImg.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadData();

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        tv_downloadImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            requestPermissionAndContinue();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();

            }
        });
    }

    private void loadData() {
        if (appName != null) {
            app_name.setText(appName);
            tv_appName.setText(appName);
        }
        if (imageURL != null) {
            Glide.with(getActivity()).load(imageURL).into(app_img);
        }
        if (appType != null) {
            tv_appType.setText(appType);
        }
        if (storeLink != null) {
            tv_appStoreLink.setText(storeLink);
        }
        if (appDetails != Double.NaN) {
            if (appType.equals("ios"))
                tv_appDetails.setText(" - " + getString(R.string.app_size_tittle) + " " + String.valueOf(appDetails));
            else {
                int ratings = (int) appDetails;
                tv_appDetails.setText(" - " + getString(R.string.app_ratings_tittle) + " " + String.valueOf(ratings));
            }
        }
        if (appDetails2 != 0.0) {
            tv_appDetails2.setText(" - " + getString(R.string.app_score_tittle) + " " + String.valueOf(appDetails2));
        } else {
            tv_appDetails2.setText("");
        }
    }

    private boolean checkPermission() {

        return ContextCompat.checkSelfPermission(getContext(), WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getContext(), READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ;
    }

    public void downloadImage() {
        URL url = null;
        String fileName = "";
        try {
            url = new URL(imageURL);
            int id = imageURL.lastIndexOf("/");
            fileName = imageURL.substring(id + 1, imageURL.length());
            //Log.d("SANDI", "filename: " + fileName);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        InputStream input = null;
        try {
            input = url.openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            File storagePath = Environment.getExternalStorageDirectory(); //Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            OutputStream output = null;
            try {
                output = new FileOutputStream(new File(storagePath, fileName));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                byte[] buffer = new byte[1024];
                int bytesRead = 0;
                while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                    output.write(buffer, 0, bytesRead);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    output.close();
                    Utils.showSnack(getView(), getString(R.string.success_download_text) + " \n " + storagePath.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                    Utils.showSnack(getView(), getString(R.string.niot_succes_download_text) + " \n " + storagePath.getAbsolutePath());

                }
            }
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void requestPermissionAndContinue() {

        final PermissionEnum permissionStorage = PermissionEnum.WRITE_EXTERNAL_STORAGE;
        final PermissionEnum permissionReadStorage = PermissionEnum.READ_EXTERNAL_STORAGE;
        boolean grantedStorage = PermissionUtils.isGranted(getActivity(), permissionStorage);
        boolean grantedReadStorage = PermissionUtils.isGranted(getActivity(), permissionReadStorage);

        if (grantedStorage && grantedReadStorage) {
            downloadImage();
        } else {
            if (!grantedStorage) {
                PermissionManager.Builder()
                        .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE, PermissionEnum.READ_EXTERNAL_STORAGE, PermissionEnum.CAMERA)
                        .askAgain(true)
                        .askAgainCallback(new AskAgainCallback() {
                            @Override
                            public void showRequestPermission(UserResponse response) {
                                PermissionUtils.openApplicationSettings(getActivity(), R.class.getPackage().getName());
                            }
                        })
                        .callback(new FullCallback() {
                            @Override
                            public void result(ArrayList<PermissionEnum> permissionsGranted, ArrayList<PermissionEnum> permissionsDenied, ArrayList<PermissionEnum> permissionsDeniedForever, ArrayList<PermissionEnum> permissionsAsked) {
                            }
                        })
                        .ask(this);
            }
        }
    }
}
