package com.example.mobile_application_test_app;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.os.ConfigurationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.mobile_application_test_app.R;
import com.example.mobile_application_test_app.fragments.AppShowFragment;
import com.example.mobile_application_test_app.fragments.HomeFragment;
import com.example.mobile_application_test_app.remote.Models.Page;
import com.example.mobile_application_test_app.remote.REST;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    List<String> fragmentIndex = new ArrayList<>();
    REST.getApps getAppCall;
    Retrofit retrofit;
    Page page;
    AlertDialog.Builder alertDialog;
    String app_language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        alertDialog = new AlertDialog.Builder(this);
        loadData();
        setContentView(R.layout.activity_main);

        Locale primaryLocale = null;
        ConfigurationCompat.getLocales(Resources.getSystem().getConfiguration());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            primaryLocale = getApplicationContext().getResources().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            primaryLocale = Resources.getSystem().getConfiguration().locale;
        }
        String locale = primaryLocale.getDisplayName();
        //addFragment(HomeFragment.class, null, false, true);
    }

    public void addFragment(Class fragmentClass, Bundle b, boolean addToBackstack, boolean replace) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (replace) {
            if (fragmentManager.getBackStackEntryCount() > 0)
                fragmentManager.popBackStack();
        }

        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (b != null && fragment != null)
            fragment.setArguments(b);
        // Insert the fragment by replacing any existing fragment
        FragmentTransaction ft = fragmentManager.beginTransaction();

        ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

        if (addToBackstack)
            ft.addToBackStack(fragmentClass.getSimpleName());

        try {
            if (replace) {
                ft.replace(R.id.frame, fragment, fragmentClass.getSimpleName()).commitAllowingStateLoss();
                fragmentIndex.clear();
            } else {
                ft.add(R.id.frame, fragment, fragmentClass.getSimpleName()).commitAllowingStateLoss();
            }

            fragmentIndex.add(fragmentClass.getSimpleName());
        } catch (Exception ignored) {
        }
    }

    void loadData() {

        retrofit = REST.getClient();

        getAppCall = retrofit.create(REST.getApps.class);

        Call<Page> myApps = getAppCall.getAppsByPage(1);

        myApps.enqueue(new Callback<Page>() {
            @Override
            public void onResponse(Call<Page> call, Response<Page> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    page = response.body();
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.MAXPAGE, page.getMaxPages());
                    addFragment(HomeFragment.class, bundle, false, true);
                } else if (response.code() == 500) {

                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setTitle(getString(R.string.no_loaded_warning));
                    alertDialog.setMessage(getString(R.string.instructions_for_data));
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            loadData();
                        }
                    });
                    alertDialog.show();
                }
            }

            @Override
            public void onFailure(Call<Page> call, Throwable t) {
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setTitle(getString(R.string.no_loaded_warning));
                alertDialog.setMessage(getString(R.string.instructions_for_data));
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        loadData();
                    }
                });
                alertDialog.show();
            }
        });
    }
}
