package com.example.mobile_application_test_app.remote.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AndroidDetails {

    @SerializedName("ratings")
    @Expose
    private Integer ratings;
    @SerializedName("score")
    @Expose
    private Double score;

    public Integer getRatings() {
        return ratings;
    }

    public void setRatings(Integer ratings) {
        this.ratings = ratings;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

}
