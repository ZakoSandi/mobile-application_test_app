package com.example.mobile_application_test_app.remote.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("ios_store")
    @Expose
    private String iosStore;
    @SerializedName("ios_details")
    @Expose
    private IosDetails iosDetails;
    @SerializedName("android_store")
    @Expose
    private String androidStore;
    @SerializedName("android_details")
    @Expose
    private AndroidDetails androidDetails;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIosStore() {
        return iosStore;
    }

    public void setIosStore(String iosStore) {
        this.iosStore = iosStore;
    }

    public IosDetails getIosDetails() {
        return iosDetails;
    }

    public void setIosDetails(IosDetails iosDetails) {
        this.iosDetails = iosDetails;
    }

    public String getAndroidStore() {
        return androidStore;
    }

    public void setAndroidStore(String androidStore) {
        this.androidStore = androidStore;
    }

    public AndroidDetails getAndroidDetails() {
        return androidDetails;
    }

    public void setAndroidDetails(AndroidDetails androidDetails) {
        this.androidDetails = androidDetails;
    }

}
